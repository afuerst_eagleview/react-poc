import React, { Component } from 'react';
import { Grid, Menu, Header } from 'semantic-ui-react';
import AccountMenu from './account-menu.js'

class AppHeader extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            activeItem: 'Captures'
        }
        
        this.props = {
            activeUser: 'Unknown User',
            onUserChange: undefined
        }
    }

    handleItemClick = (e, { name }) => this.setState({ activeItem: name })
    
    render() {
        return (
            <Grid columns={2}>
                <Grid.Row>
                    <Grid.Column width={14} floated='left'>
                        <Header size='huge'> Castle </Header>
                    </Grid.Column>
                    <Grid.Column width={2} floated='right'>
                        <AccountMenu 
                            activeUser={this.props.activeUser} 
                            onUserChange={this.props.onUserChange}
                        />
                    </Grid.Column>
                </Grid.Row>
                
                <Grid.Row>
                    <Grid.Column width={16} floated='left'>
                        <Menu>
                            <Menu.Item
                                name='Captures'
                                active={this.state.activeItem === 'Captures'}
                                onClick={this.handleItemClick}
                            >
                                Captures
                            </Menu.Item>
                            
                            <Menu.Item
                                name='Surveys'
                                active={this.state.activeItem === 'Surveys'}
                                onClick={this.handleItemClick}
                            >
                                Surveys
                            </Menu.Item>
                            
                            <Menu.Item
                                name='Copy'
                                active={this.state.activeItem === 'Copy'}
                                onClick={this.handleItemClick}
                            >
                                Copy
                            </Menu.Item>
                            
                            <Menu.Item
                                name='Process'
                                active={this.state.activeItem === 'Process'}
                                onClick={this.handleItemClick}
                            >
                                Process
                            </Menu.Item>
                            
                            <Menu.Item
                                name='Publish'
                                active={this.state.activeItem === 'Publish'}
                                onClick={this.handleItemClick}
                            >
                                Publish
                            </Menu.Item>
                        </Menu>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}

export default AppHeader;