import React, { Component } from 'react';
import { Dropdown } from 'semantic-ui-react';

class AccountMenu extends Component {
    constructor(props) {
        super(props);
        
        this.props = {
            activeUser: '',
            onUserChange: undefined
        }
    }
    
    handleUserChangeClick(e) {
        if(this.props.onUserChange !== undefined) {
            this.props.onUserChange('stuff');
        }
    }
    
    render() {
        return (
            <Dropdown text={this.props.activeUser} >
                <Dropdown.Menu>
                    <Dropdown.Item 
                        text='Change User'
                        onClick={this.handleUserChangeClick.bind(this)}
                    />
                    <Dropdown.Item 
                        text='Not Implemented'
                    />
                </Dropdown.Menu>
            </Dropdown>
        );
    }
}

export default AccountMenu;