import React, { Component } from 'react';
import AppHeader from './partials/header.js';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeUser: 'Alex Fuerst'
        }
    }
    
    handleUserChange(newUser) {
        this.setState({activeUser: newUser});
    }
    
    render() {
        return (
            <div className="App">
                <AppHeader 
                    activeUser={this.state.activeUser}
                    onUserChange={this.handleUserChange.bind(this)}>
                </AppHeader>
            </div>
        );
    }
}

export default App;
