## Summary ##
This application is a quick demonstration of a modular single page application that has real-time state.
Transpiling, bundling, linting, server is all handled by the create react app utility. Without this we would need to implement each of these ourselves.
yarn/npm install and yarn/npm start should get you up and running.
## Technologies ##
 - [React.js](https://reactjs.org/tutorial/tutorial.html#getting-started) - Shadow DOM (fast updates) Stateful UI, great single page applications
	 - JSX - XML module format for writing React Components (concise and readable)
	 - ES6 - 2015 JS Standard (Promises, classes, modules)
 - [Create React App](https://github.com/facebookincubator/create-react-app)
	 - [Webpack](https://webpack.js.org/) - Bundling utility 
		 - [webpack-dev-server](https://github.com/webpack/webpack-dev-server) - Runs a server for us and reloads when project has changes (nice for node based servers)
		 - [html-webpack-plugin](https://github.com/jantimon/html-webpack-plugin) - plugin that injects bundled and transpiled files into a given html template.
	 - [ESLint](https://eslint.org/) - typical linter, but used in realtime by create react app
 - [Semantic UI - React](https://react.semantic-ui.com/introduction) - A nice library for declaring UI in React Applications. It removes Bootstrap and JQuery Needs
 - [Babel](http://babeljs.io/) - a transpiler that converts JSX and ES6 into ES5 so that modern browsers can interpret it.
## Considerations ##
 - PHP Server and Routing - Will need to work out how to incorporate babel and webpack into our build cycle
 - Multipage application - will need to use multiple render points and multiple html templates
 - Source Mapping - Chrome and Firefox can map ES6 source code to the transpiled code so that you can put break points into your es6 code while still testing in the browser.
 - React Router?
 - Switch to Node Server and utilize webpack and yarn for client and server package management
## Additional Reading ##
 - https://scotch.io/tutorials/setup-a-react-environment-using-webpack-and-babel#getting-started
 - https://medium.com/@pshrmn/a-simple-react-router-v4-tutorial-7f23ff27adf
 - https://yarnpkg.com/en/docs
 - https://www.tutorialspoint.com/es6/es6_promises.htm
 - https://www.codementor.io/tamizhvendan/beginner-guide-setup-reactjs-environment-npm-babel-6-webpack-du107r9zr